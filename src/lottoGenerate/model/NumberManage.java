package lottoGenerate.model;

import java.util.ArrayList;
import java.util.List;

public class NumberManage {
	private List<Number> lists;
	
	public NumberManage() {
		this.lists = new ArrayList<Number>();
	}
	
	public boolean contain(Number number) {
		if (number == null) {
			throw new IllegalArgumentException("number should not be null");
		}
		return this.lists.contains(number);
	}
	public boolean add(Number number) {
		if (this.lists.add(number)) {
			return true;
		}
		return false;
	}
	public boolean removeByTurn(String number) {
		for (Number current : this.lists) {
			if (current.getTurn().equals(number)) {
				return this.lists.remove(current);
			}
		}
		return false;
	}
	public boolean containByTurn(String number) {
		for (Number current: this.lists) {
			if (current.getTurn().equals(number)) {
				return true;
			}
		}
		return false;
	}
	public String findHighestFrequency() {
		String result = "";
		int[][] totalChance = new int[7][45];
		for (int index = 0; index < this.lists.size(); index++) {
			for (int count = 0; count <= 6; count++) {
				totalChance[count][this.lists.get(index).getNumberByIndex(count) - 1]++;
			}
		}
		
		int[] highestFrequency = new int[7];
		for (int index = 0; index <= 6; index++) {
			int frequency = 0;
			for (int count = 0; count <= 44; count++) {
				if (totalChance[index][count] >= frequency) {
					highestFrequency[index] = count + 1;
					frequency = totalChance[index][count];
				}
			}
		}
		
		for (int number = 0; number <= 6; number++) {
			result += "Highest Chance of " + (number + 1) + " number(s) is :" + highestFrequency[number];
		}
		return result;
	}
}
