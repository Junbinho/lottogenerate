package lottoGenerate.model;

public class Number {
	private String turn;
	private int[] numbers;
	
	public Number(String turnName, int[] numberName) {
		this.numbers = new int[7];
		if (turnName.isEmpty() || turnName == null) {
			throw new IllegalArgumentException("name should not be null or empty");
		}
		if (numberName == null) {
			throw new IllegalArgumentException("number should not be null");
		}
		this.turn = turnName;
		this.numbers = numberName;
	}

	public String getTurn() {
		return this.turn;
	}

	public void setTurn(String turn) {
		if (turn.isEmpty() || turn == null) {
			throw new IllegalArgumentException("name should not be null or empty.");
		}
		this.turn = turn;
	}

	public int[] getNumbers() {
		return this.numbers;
	}
	public int getNumberByIndex(int index) {
		return this.numbers[index];
	}

	public void setNumbers(int[] numbers) {
		if (numbers == null) {
			throw new IllegalArgumentException("number should not be null");
		}
		this.numbers = numbers;
	}
}
