package lottoGenerate.view;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class LottoGenerateGuiCodeBehind {

    @FXML
    private TextField numbersTextField;

    @FXML
    private Button generateButton;

    @FXML
    private Button compareButton;

    @FXML
    private Button cheatButton;

    @FXML
    private TextArea generateArea;

    @FXML
    private TextArea percentageArea;

    @FXML
    void handleCheat(ActionEvent event) {

    }

    @FXML
    void handleCompare(ActionEvent event) {

    }

    @FXML
    void handleGenerate(ActionEvent event) {

    }

}

